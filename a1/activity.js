//insertOne

	db.hotel.insert({
		name: "single",
		accommodates: 2,
		price: 1000,
		description: "A simple room with all the basic necessities.",
		rooms_available: 10,
		isAvailable: false
	})

//insertMany

	db.hotel.insertMany([
		{
			name: "double",
			accommodates: 3,
			price: 2000,
			description: "A room fit for a small family going on a vacation.",
			rooms_available: 5,
			isAvailable: false
		},
		{
			name: "queen",
			accommodates: 4,
			price: 4000,
			description: "A room with queen sized bed perfect for a simple getaway.",
			rooms_available: 15,
			isAvailable: false
		}
	])

//Find Double Room

	db.hotel.find({name: "double"});

//updateOne

	db.hotel.updateOne(
		{name: "queen"},
		{
			$set: {
				name: "queen",
				accommodates: 4,
				price: 4000,
				description: "A room with queen sized bed perfect for a simple getaway.",
				rooms_available: 0,
				isAvailable: false
			}
		}
	)


//deleteAll

	db.hotel.deleteMany({rooms_available: 0})