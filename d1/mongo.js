//CRUD Operations
/*
	CRUD Operations are the heart of any back end application.


*/


//Insert Documents (Create)

//Inserting one document

	//Syntax
		//db.collectionName.insertOne({object});
		//db.collectionName.insert({object});

	//JavaScript syntax comparison
		//object.object.method({object});

	db.users.insert({
	    firstName: "Jane",
	    lastName: "Doe",
	    age: 21,
	    contact: {
	        phone: "87654321",
	        email: "janedoe@gmail.com"
	    },
	    courses: ["CSS", "JavaScript", "Python"],
	    department: "none"
	});

	//Insert Many

	//Syntax
		//db.collectionName.insertMany([{objectA}, {objectB}]);

	db.users.insertMany([
		{
			firstName: "Stephen",
			lastName: "Hawking",
			age: 76,
			contact:{
				phone: "87654321",
				email: "stephenhawking@gmail.com"
			},
			courses: ["Python", "React", "PHP"],
			department: "none"
		},
		{
			firstName: "Neil",
			lastName: "Armstrong",
			age: 82,
			contact:{
				phone: "87654321",
				email: "neilarmstrong@gmail.com"
			},
			courses: ["React", "Laravel", "Sass"],
			department: "none"
		}
	]);

	//Finding Documents (Read/Retrieve)

	//Find
	/*
		- if multiple documents match the criteria for finding a document only the FIRST document that matches the search term will be returned
		- this is also based from the order that documents are stored in a collection
		- if the document is not found, the terminal will respond with a blank line

		Syntax:
			db.collectionName.find();
			db.collectionName.find({field:value});

	*/

	//Finding a single document
	//Leaving the search criteria empty will retrieve ALL documents

	db.users.find();

	db.users.find({firstName:"Stephen"});

	//Finding documents with multiple parameters

	/*
		Syntax:
			db.collectionsName.find({fieldA: value, fieldB: valueB});

	*/

	//Mini Activity
		//Find a document with the lastName: "Armstrong", age: 82
		//Execute in Robo3T

		db.users.find({lastName: "Armstrong", age: 82});

	//Updating Documents

	//update a single document

	//***********create a document that we will update

	db.users.insert({

		firstName: "Test",
		lastName: "Test",
		age: 0,
		contact: {
			phone: "00000000",
			email: "test@gmail.com"
		},
		courses: [],
		department: "none"
	});


	/*
		Just like the "find" method, methods that only manipulate a single document will only update the FIRST document that matches the search criteria

			Syntax:

				db.collectionName.updateOne({criteria},{$set: {field:value}});
	*/

	db.users.updateOne(
		{firstName: "Test"},
		{
			$set: {
				firstName: "Bill",
				lastName: "Gates",
				age: 65,
				contact:{
					phone: "12345678",
					email: "bill@gmail.com"
				},
				courses: ["PHP", "Laravel", "HTML"],
				department: "Operations",
				status: "active"
			}
		}
	);

	//Updating Multiple Documents

	/*
		Syntax:
			db.collectionName.updateMany({criteria},{$set:{field:value}});

	*/


	db.users.updateMany(
		{department: "none"},
		{
			$set:{department:"HR"}
		}
	);


	//replaceOne

	/*
		Can be used if replacing the whole document is necessary

		Syntax:

			db.collectionName.replaceOne({criteria},{{field:value}});

	*/

	db.users.replaceOne(

			{firstName: "Bill"},
			{
				firstName: "Bill",
				lastName: "Gates",
				age: 65,
				contact:{
					phone: "12345678",
					email: "bill@gmail.com"
				},
				courses: ["PHP", "Laravel", "HTML"],
				department:"Operations"
			}

		);

				//in effect, status was removed in the whole document



	//Deleting Documents (Delete)

	//creating a document to delete

	db.users.insert({
		firstName: "Test"
	});


	//Delete a single document

	/*
		Syntax:

			db.collectionName.deleteOne({criteria});
	*/


		db.users.deleteOne({
			firstName: "Test"
		});


	//Delete Many

	/*
		Syntax:

			db.collectionName.deleteMany({criteria});
	*/

		db.users.deleteMany({
			firstName: "Bill"
		});


	//Advanced Queries

	//Query and embedded document

		db.users.find({
			contact:{
				phone: "87654321",
				email: "stephenhawking@gmail.com"
			}
		});

	//Query on a nested field

		db.users.find({
			"contact.email": "janedoe@gmail.com"
		});

	//Querying an Array with Exact Elements

		db.users.find({courses: ["CSS", "JavaScript", "Python"]});

	//Querying an Array without regard to order

		db.users.find({ courses:{ $all: ["React", "Python"] } });

	//Querying an Embedded Array

		//Added another document
		db.users.insert({

				nameArr: [

				{
					namea: "juan"
				},
				{
					nameb: "tamad"
				}
			]
		});

		db.users.find({
			namearr:
			{
				namea: "juan"
			}
		})